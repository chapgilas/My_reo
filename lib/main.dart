import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String appbartext = "خانه";
  Icon mahdiyar = Icon(Icons.home);
  void onPressed() {
    setState(() {
      this.appbartext = "خانه ما";
      this.mahdiyar = Icon(Icons.play_arrow);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(appbartext),
          centerTitle: true,
          actions: [
            IconButton(onPressed: onPressed, icon: Icon(Icons.home)),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: onPressed,
          child: mahdiyar,
        ),
      ),
    );
  }
}
